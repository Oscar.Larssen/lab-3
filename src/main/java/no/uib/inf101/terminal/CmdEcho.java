package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {

        String echostring = "";

        for (String string : args) {

            echostring += string + " "; 
            

        }

        return echostring;
    }

    @Override
    public String getName() {
        return  "echo";
    }
    
}
