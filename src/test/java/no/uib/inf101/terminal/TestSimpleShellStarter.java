package no.uib.inf101.terminal;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSimpleShellStarter {
    static final String orgCwd = System.getProperty("user.dir");
    static final String DIR = "testdir";
    static final String SUBDIR = "subdir";
    private File dir;
    private File subdir;
    private SimpleShell shell;

    ////////////////////////////////////////////////////////////////////////
    //////// The tests ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Test
    public void testDoNothing() {
        assertEquals("$ ", shell.getScreenContent());
    }

 /*    @Test
    public void testWriteFoo() {
        if ('f' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('f' >= ' ' && 'f' <= '~') {
            shell.currentCommand += 'f';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('o' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('o' >= ' ' && 'o' <= '~') {
            shell.currentCommand += 'o';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('o' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('o' >= ' ' && 'o' <= '~') {
            shell.currentCommand += 'o';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }

        assertEquals("$ foo", shell.getScreenContent());
    }

    @Test
    public void testIllegalCommand() {
        if ('f' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('f' >= ' ' && 'f' <= '~') {
            shell.currentCommand += 'f';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('o' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('o' >= ' ' && 'o' <= '~') {
            shell.currentCommand += 'o';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('o' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('o' >= ' ' && 'o' <= '~') {
            shell.currentCommand += 'o';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('\n' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('\n' >= ' ' && '\n' <= '~') {
            shell.currentCommand += '\n';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }

        assertEquals("$ foo\nCommand not found: \"foo\"\n$ ", shell.getScreenContent());
    }

    @Test
    public void testPwd() throws IOException {
        if ('p' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('p' >= ' ' && 'p' <= '~') {
            shell.currentCommand += 'p';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('w' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('w' >= ' ' && 'w' <= '~') {
            shell.currentCommand += 'w';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('d' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('d' >= ' ' && 'd' <= '~') {
            shell.currentCommand += 'd';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('\n' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('\n' >= ' ' && '\n' <= '~') {
            shell.currentCommand += '\n';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }

        String expected = "$ pwd" + "\n" + this.dir.getCanonicalPath() + "\n$ ";
        assertEquals(expected, shell.getScreenContent());
    }

    @Test
    public void testLs() throws IOException {
        if ('l' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('l' >= ' ' && 'l' <= '~') {
            shell.currentCommand += 'l';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('s' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('s' >= ' ' && 's' <= '~') {
            shell.currentCommand += 's';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('\n' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('\n' >= ' ' && '\n' <= '~') {
            shell.currentCommand += '\n';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }

        String expected = "$ ls\n" + SUBDIR + " \n$ ";
        assertEquals(expected, shell.getScreenContent());
    }

    @Test
    public void testCd() throws IOException {
        if ('p' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('p' >= ' ' && 'p' <= '~') {
            shell.currentCommand += 'p';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('w' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('w' >= ' ' && 'w' <= '~') {
            shell.currentCommand += 'w';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('d' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('d' >= ' ' && 'd' <= '~') {
            shell.currentCommand += 'd';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('\n' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('\n' >= ' ' && '\n' <= '~') {
            shell.currentCommand += '\n';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }

        if ('c' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('c' >= ' ' && 'c' <= '~') {
            shell.currentCommand += 'c';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('d' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('d' >= ' ' && 'd' <= '~') {
            shell.currentCommand += 'd';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if (' ' == '\n') {
            shell.processCurrentCommandLine();
        } else if (' ' >= ' ' && ' ' <= '~') {
            shell.currentCommand += ' ';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('s' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('s' >= ' ' && 's' <= '~') {
            shell.currentCommand += 's';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('u' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('u' >= ' ' && 'u' <= '~') {
            shell.currentCommand += 'u';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('b' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('b' >= ' ' && 'b' <= '~') {
            shell.currentCommand += 'b';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('d' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('d' >= ' ' && 'd' <= '~') {
            shell.currentCommand += 'd';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('i' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('i' >= ' ' && 'i' <= '~') {
            shell.currentCommand += 'i';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('r' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('r' >= ' ' && 'r' <= '~') {
            shell.currentCommand += 'r';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('\n' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('\n' >= ' ' && '\n' <= '~') {
            shell.currentCommand += '\n';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }

        if ('p' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('p' >= ' ' && 'p' <= '~') {
            shell.currentCommand += 'p';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('w' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('w' >= ' ' && 'w' <= '~') {
            shell.currentCommand += 'w';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('d' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('d' >= ' ' && 'd' <= '~') {
            shell.currentCommand += 'd';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }
        if ('\n' == '\n') {
            shell.processCurrentCommandLine();
        } else if ('\n' >= ' ' && '\n' <= '~') {
            shell.currentCommand += '\n';
        } else {
            // Some special key was pressed (e.g. shift, ctrl), we ignore it
        }

        String expected = "$ pwd\n"
                + this.dir.getCanonicalPath() + "\n"
                + "$ cd subdir\n"
                + "$ pwd\n"
                + this.subdir.getCanonicalPath() + "\n"
                + "$ ";

        assertEquals(expected, shell.getScreenContent());
    }
 */
    ////////////////////////////////////////////////////////////////////////
    //////// Preparing the tests //////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @BeforeEach
    public void setUp() throws IOException {
        // Set up test directory
        File cwd = new File(orgCwd);
        this.dir = new File(cwd, DIR);
        this.subdir = new File(this.dir, SUBDIR);
        this.dir.mkdir();
        this.subdir.mkdir();
        System.setProperty("user.dir", this.dir.getCanonicalPath());
        this.shell = new SimpleShell();
    }

    @AfterEach
    public void tearDown() {
        File cwd = new File(System.getProperty("user.dir"));
        File testDir = new File(cwd, DIR);
        deleteFolderAndItsContent(testDir);
        System.setProperty("user.dir", orgCwd);
        this.shell = null;
    }

    private void deleteFolderAndItsContent(File file) {
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                deleteFolderAndItsContent(f);
            }
        }
        file.delete();
    }

}
